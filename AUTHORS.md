# Authors

Jonathan Baginski ([patchedsoul](https://gitlab.com/patchedsoul))

This project is based on the Rust game engine [Amethyst](https://amethyst.rs). 
Thanks to the [amethyst development community](https://amethyst.rs/team) for all your hard work!

