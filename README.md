# sardonyx Tools

Command-line interface to create and deploy sardonyx automation projects. 

## Installing

By executing

```sh
cargo install sardonyx_tools
```

a binary called `sardonyx` will be placed in your `~/cargo/bin` folder.

## Usage

### Creating a new project

```sh
sardonyx new <project_name>
```

## License
This project is based on the Rust game engine [Amethyst](https://amethyst.rs). Thanks to the [amethyst development community](https://amethyst.rs/team) for all your hard work!

sardonyx is free and open source software distributed under the terms of both the [MIT License][lm] and the [Apache License 2.0][la].

[lm]: LICENSE-MIT
[la]: LICENSE-APACHE

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
